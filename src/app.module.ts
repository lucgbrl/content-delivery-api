import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AppController } from './app.controller';
import { ProdutosController } from './produtos.controller';
import { AppService } from './app.service';
import { ProdutosService } from './produtos.service';
import { BooksController } from './books/books.controller';
import { Produto } from './produto.model';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'domingo15',
      database: 'catalogo',
      autoLoadModels: true,
      synchronize: true
    }),
    SequelizeModule.forFeature([Produto])
  ],
  controllers: [AppController, ProdutosController, BooksController],
  providers: [AppService, ProdutosService],
})
export class AppModule {}
