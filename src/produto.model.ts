import { Column, DataType, Model } from "sequelize-typescript";
import { Table } from "sequelize-typescript";

@Table
export class Produto extends Model{

    @Column({
        type: DataType.STRING(60),
        allowNull: false
    })
    codigo: string;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    nome: string; 
    
    @Column({
        type: DataType.DECIMAL(10,2)
    })
    preco: number;      

}